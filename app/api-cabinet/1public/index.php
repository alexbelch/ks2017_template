<?php

header('Content-Type: application/json');

echo json_encode([
    'error'   => false,
    'message' => 'ok',
    'type'    => 'cabinet'
]);

exit(0);
